#!/usr/bin/env bash

_pwgen() {
	count=$1
	length=$2                                            

	while [ $count -gt 0 ]; do
	tr -dc A-Za-z0-9_ < /dev/urandom | head -c $length | xargs 
	count=$(($count-1))
	done
}

_pwcount() {
	for password in $passwords; do
	echo $password | tr -cd "[:digit:]" | xargs |\
	tr "[:digit:]" '1' | fold -w1 | paste -sd+ - | bc
	done
}

_pwprint() {
	while read count digits; do
        echo "Number of passwords with $digits digits = $count"  
	done < <(_pwcount|sort|uniq -c)
}

passwords=$(_pwgen 50 40)
_pwprint


